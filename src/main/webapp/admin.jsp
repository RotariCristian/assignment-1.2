<%--
  Created by IntelliJ IDEA.
  User: Brazzar
  Date: 30.10.2017
  Time: 00:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-10">
            <h1>Admin Page</h1>
<form action="${pageContext.request.contextPath}/admin" method="post">

<div class="form-group">
    <label class="control-label">Flight Number</label>
    <input type="text" name="flightNumber" class="form-control input-sm chat-input" placeholder="Flight Number" />
</div>

    <div class="form-group">
        <label class="control-label">Airplane Type</label>
        <input type="text" name="airplaneType" class="form-control input-sm chat-input" placeholder="Airplane Type" />
    </div>

    <div class="form-group">
        <label class="control-label">Departure City</label>
        <input type="text" name="departureCity" class="form-control input-sm chat-input" placeholder="Departure City" />
    </div>

    <div class="form-group">
        <label class="control-label">Departure Time</label>
        <input type="text" name="departureTime" class="form-control input-sm chat-input" placeholder="Departure Time" />
    </div>

    <div class="form-group">
        <label class="control-label">Arrival City</label>
        <input type="text" name="arrivalCity" class="form-control input-sm chat-input" placeholder="Arrival City" />
    </div>

    <div class="form-group">
        <label class="control-label">Arrival Time</label>
        <input type="text" name="arrivalTime" class="form-control input-sm chat-input" placeholder="Arrival Time" />
    </div>

    <input  class="btn success" type="submit" name="create" value="Create" />

    <input  class="btn info" type="submit" name="update" value="Update" />

</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Flight Number</th>
        <th>Airplane Type</th>
        <th>Departure City</th>
        <th>Departure Time</th>
        <th>Arrival City</th>
        <th>Arrival Time</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
    <div class="container-fluid">
        <c:forEach var ="flight" items ="${flightList}" >
            <tr>
            <tr>
                <td>${flight.flightNumber}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.departureCity.name}</td>
                <td>${flight.departureTime}</td>
                <td>${flight.arrivalCity.name}</td>
                <td>${flight.arrivalTime}</td>
                <td>
                    <form action="/admin" method="post">
                            <input  class="btn danger" type="submit" name="delete" value="Delete" />
                        <input type="text" name="flightId"  value = "${flight.id}" hidden />
                    </form>

                </td>
            </tr>
        </c:forEach>
    </div>
    </tbody>
</table>
    </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>