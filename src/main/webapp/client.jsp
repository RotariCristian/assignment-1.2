<%--
  Created by IntelliJ IDEA.
  User: Brazzar
  Date: 30.10.2017
  Time: 00:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Client</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-10">
            <h1>Client Page</h1>
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Flight Number</th>
        <th>Airplane Type</th>
        <th>Departure City</th>
        <th>Departure Time</th>
        <th>Arrival City</th>
        <th>Arrival Time</th>
        <th>Local</th>
    </tr>
    </thead>
    <tbody>
    <div class="container-fluid">
    <c:forEach var ="flight" items ="${flightList}" >
        <tr>
            <tr>
                <td>${flight.flightNumber}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.departureCity.name}</td>
                <td>${flight.departureTime}</td>
                <td>${flight.arrivalCity.name}</td>
                <td>${flight.arrivalTime}</td>
                <td>
                    <a href="<c:url value='/client?action=local&id=${flight.id}' />">
                        <span class="	glyphicon glyphicon-time"></span>
                    </a>
                </td>
        </tr>
    </c:forEach>
        <br>
        <c:if test="${showlocal = true}">
            <p> Departure time in arrival city : ${arrivalLocal}</p>
            <p> Arrival time in departure city : ${departureLocal}</p>
        </c:if>

        </div>
    </tbody>
</table>

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>