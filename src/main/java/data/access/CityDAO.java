package data.access;

import model.City;

public interface CityDAO {

    City findByName(String name);
}
