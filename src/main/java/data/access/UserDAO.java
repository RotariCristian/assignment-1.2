package data.access;

import model.User;

public interface UserDAO {

    User findByCredentials(String username, String password);
}
