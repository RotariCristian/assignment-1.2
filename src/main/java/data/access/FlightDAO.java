package data.access;

import model.Flight;

import java.util.List;

public interface FlightDAO {

    Flight saveFlight(Flight flight);

    Flight updateFlight(Flight flight);

    Flight getFlightById(Integer flightId);

    void deleteFlight(Flight flight);

    List<Flight> getAllFlights();

    Flight getFlightByFlightNumber(Integer flightNumber);
}
