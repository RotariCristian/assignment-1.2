package data.access;

import model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class UserDAOImp implements UserDAO{

    private SessionFactory factory;

    public UserDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    @Override
    public User findByCredentials(String username, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        User u=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username and password = :password");
            query.setParameter("username", username );
            query.setParameter("password", password );
            List<User> result = query.getResultList();
            if(result!=null && result.size()>0)
                u = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return u;
    }
}
