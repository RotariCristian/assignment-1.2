package data.access;

import model.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class CityDAOImp implements CityDAO {

    private SessionFactory factory;

    public CityDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    @Override
    public City findByName(String name) {
        Session session = factory.openSession();
        Transaction tx = null;
        City c=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", name );
            List<City> result = query.getResultList();
            if(result!=null && result.size()>0)
                c = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return c;
    }
}
