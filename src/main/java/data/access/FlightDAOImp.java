package data.access;

import business.FlightService;
import business.FlightServiceImp;
import model.City;
import model.Flight;
import model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightDAOImp implements FlightDAO{

    private SessionFactory factory;

    public FlightDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    public Flight saveFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight f=null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return f;
    }

    public Flight updateFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight f=null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return f;
    }

    public Flight getFlightById(Integer flightId) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight f=null;
        try {
            tx = session.beginTransaction();
            f = session.get(Flight.class, flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return f;
    }

    public Flight getFlightByFlightNumber(Integer flightNumber) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight f=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber );
            List<Flight> result = query.getResultList();
            if(result!=null && result.size()>0)
                f = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return f;
    }

    public void deleteFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public List<Flight> getAllFlights() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = new ArrayList<Flight>();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Flight");
            flights = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights;
    }
}
