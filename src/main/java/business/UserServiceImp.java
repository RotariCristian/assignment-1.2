package business;

import data.access.UserDAO;
import model.User;

public class UserServiceImp implements UserService {

    private UserDAO userDAO;

    public UserServiceImp(UserDAO userDAO){
        this.userDAO = userDAO;
    }
    @Override
    public User findByCredentials(String username, String password) {
        return this.userDAO.findByCredentials(username,password);
    }
}
