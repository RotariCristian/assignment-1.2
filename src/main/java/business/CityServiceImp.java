package business;

import data.access.CityDAO;
import model.City;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class CityServiceImp implements CityService {

    private CityDAO cityDAO;

    public CityServiceImp(CityDAO cityDAO){
        this.cityDAO = cityDAO;
    }

    @Override
    public City findByName(String name) {
        return this.cityDAO.findByName(name);
    }

    @Override
    public Integer getTimezone(double latitude, double longitude) {

            String urlStr = "http://new.earthtools.org/timezone/" + latitude + "/" + longitude;

        try {
            URL url = new URL(urlStr);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/xml");

            InputStream xml = connection.getInputStream();

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(xml);
            NodeList localTime = document.getElementsByTagName("offset");

            return Integer.parseInt(localTime.item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
