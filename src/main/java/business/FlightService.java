package business;

import model.Flight;

import java.util.Date;
import java.util.List;

public interface FlightService {

    Flight saveFlight(Flight flight);

    void deleteFlightById(Integer flightId);

    Flight getFlightById(Integer flightId);

    List<Flight> getAllFlights();

    List<Date> getFlightTimes(Flight flight);

    Flight getFlightByFlightNumber(Integer flightNumber);

    Flight updateFlight(Flight flight);
}
