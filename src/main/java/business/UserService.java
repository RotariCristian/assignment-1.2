package business;

import model.User;

public interface UserService {

    User findByCredentials(String username,String password);
}
