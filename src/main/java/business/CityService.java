package business;

import model.City;

public interface CityService {

    City findByName(String name);

    Integer getTimezone(double latitude, double longitude);

}
