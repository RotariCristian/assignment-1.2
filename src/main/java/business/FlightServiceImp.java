package business;

import data.access.FlightDAO;
import model.Flight;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FlightServiceImp implements FlightService {

    private FlightDAO flightDAO;

    public FlightServiceImp(FlightDAO flightDAO){
        this.flightDAO = flightDAO;
    }

    public Flight saveFlight(Flight flight) {
        return this.flightDAO.saveFlight(flight);
    }

    public Flight updateFlight(Flight flight) {
        return this.flightDAO.updateFlight(flight);
    }

    public void deleteFlightById(Integer flightId) {
        Flight flight = this.flightDAO.getFlightById(flightId);
        if(flight != null){
            this.flightDAO.deleteFlight(flight);
        }
    }

    public Flight getFlightById(Integer flightId) {
        return this.flightDAO.getFlightById(flightId);
    }

    public List<Flight> getAllFlights() {
        return this.flightDAO.getAllFlights();
    }

    public List<Date> getFlightTimes(Flight flight) {

        String url = "http://www.earthtools.org/timezone";
        String charset = "UTF-8";

        url+="/"+flight.getArrivalCity().getLatitude();
        url+="/"+flight.getArrivalCity().getLongitude();


        try {
            URLConnection connection = new URL(url).openConnection();
            connection.setRequestProperty("Accept-Charset", charset);
            InputStream response = connection.getInputStream();
            int status = ((HttpURLConnection)connection).getResponseCode();
            for (Map.Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
                System.out.println(header.getKey() + "=" + header.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    return null;
    }

    @Override
    public Flight getFlightByFlightNumber(Integer flightNumber) {
        return this.flightDAO.getFlightByFlightNumber(flightNumber);
    }
}
