package presentation;


import model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class LoginFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        User user;
        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = request.getRequestURI().equals("/");
        if(loggedIn){
            user = (User) session.getAttribute("user");
            if( request.getRequestURI().contains("/admin") && !user.isAdmin()){
                response.sendRedirect("/client");
                return;
            }else {
                chain.doFilter(request, response);
                return;
            }

        }

        if ( loginRequest ||  request.getRequestURI().equals("/login")) {
            chain.doFilter(request, response);
            return;
        } else {
            response.sendRedirect("/");
            return;
        }



    }

    public void destroy() {

    }


}