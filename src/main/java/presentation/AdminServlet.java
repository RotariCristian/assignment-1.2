package presentation;


import business.CityService;
import business.CityServiceImp;
import business.FlightService;
import business.FlightServiceImp;
import data.access.CityDAOImp;
import data.access.FlightDAOImp;
import model.City;
import model.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {

    private FlightService flightService = new FlightServiceImp(new FlightDAOImp());
    private CityService cityService = new CityServiceImp(new CityDAOImp());

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Flight> list = this.flightService.getAllFlights();
        request.setAttribute("flightList", list);
        request.getRequestDispatcher("admin.jsp").forward(request,response);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if ( request.getParameter("delete") != null){
            Integer flightId = Integer.parseInt(request.getParameter("flightId"));
            this.flightService.deleteFlightById(flightId);

            response.sendRedirect("/admin");
            return;
        } else {

            String flightNumber = request.getParameter("flightNumber");
            String airplaneType = request.getParameter("airplaneType");
            String departureTime = request.getParameter("departureTime");
            String departureCity = request.getParameter("departureCity");
            String arrivalTime = request.getParameter("arrivalTime");
            String arrivalCity = request.getParameter("arrivalCity");

            City depCity = this.cityService.findByName(departureCity);
            City arrivCity = this.cityService.findByName(arrivalCity);

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:MM");
            Date arrivDate = null;
            Date depDate = null;
            Integer flightNr = null;
            try {
                arrivDate = df.parse(arrivalTime);
                depDate = df.parse(departureTime);
                flightNr = Integer.parseInt(flightNumber);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Flight flight = null;

            if (request.getParameter("create") != null) {
                flight = new Flight();
                flight.setArrivalCity(arrivCity);
                flight.setDepartureCity(depCity);
                flight.setFlightNumber(flightNr);
                flight.setDepartureTime(depDate);
                flight.setArrivalTime(arrivDate);
                flight.setAirplaneType(airplaneType);
                this.flightService.saveFlight(flight);
            } else if (request.getParameter("update") != null) {
                flight = this.flightService.getFlightByFlightNumber(flightNr);
                flight.setArrivalCity(arrivCity);
                flight.setDepartureCity(depCity);
                flight.setDepartureTime(depDate);
                flight.setArrivalTime(arrivDate);
                flight.setAirplaneType(airplaneType);
                this.flightService.updateFlight(flight);
            }

            response.sendRedirect("/admin");
            return;
        }

    }

}
