package presentation;

import business.FlightService;
import business.FlightServiceImp;
import business.UserService;
import business.UserServiceImp;
import data.access.FlightDAOImp;
import data.access.UserDAOImp;
import model.Flight;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    private UserService userService = new UserServiceImp(new UserDAOImp());

    public void deGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request,response);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = this.userService.findByCredentials(username,password);

        if(user!=null){
            HttpSession session = request.getSession(false);
            session.setAttribute("user",user);
            if(user.isAdmin()){
                //request.setAttribute("user", user);
//                request.getRequestDispatcher("admin.jsp").include(request, response);
                response.sendRedirect(request.getContextPath()+"/admin");
                return;
            }else {

                response.sendRedirect(request.getContextPath()+"/client");
                return;
            }
        } else {
            request.setAttribute("error", "Invalid User");
            request.getRequestDispatcher("index.jsp").include(request, response);
        }


    }

}
