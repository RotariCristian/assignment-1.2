package presentation;

import business.CityService;
import business.CityServiceImp;
import business.FlightService;
import business.FlightServiceImp;
import data.access.CityDAOImp;
import data.access.FlightDAOImp;
import model.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = { "/client" })
public class ClientServlet extends HttpServlet {

    private FlightService flightService = new FlightServiceImp(new FlightDAOImp());
    private CityService cityService = new CityServiceImp(new CityDAOImp());

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if(request.getParameter("action")!= null) {
            Flight flight = flightService.getFlightById(Integer.parseInt(request.getParameter("id")));
            int arrivalTimezone = this.cityService.getTimezone(flight.getArrivalCity().getLatitude(), flight.getArrivalCity().getLongitude());
            int departureTimezone = this.cityService.getTimezone(flight.getDepartureCity().getLatitude(), flight.getDepartureCity().getLongitude());
            int difference = arrivalTimezone - departureTimezone;

            Calendar cal = Calendar.getInstance();

            cal.setTime(flight.getArrivalTime());
            cal.add(Calendar.HOUR_OF_DAY, -difference);
            Date arrivalLocal = cal.getTime();

            cal.setTime(flight.getDepartureTime());
            cal.add(Calendar.HOUR_OF_DAY, difference);
            Date departureLocal = cal.getTime();

            request.setAttribute("arrivalLocal", arrivalLocal);
            request.setAttribute("departureLocal", departureLocal);
            request.setAttribute("showLocal", true);
        }

            List<Flight> list = this.flightService.getAllFlights();
            request.setAttribute("flightList", list);
            request.getRequestDispatcher("client.jsp").forward(request,response);



    }

}
